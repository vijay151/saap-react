import { useState } from "react";

import "./App.css";
import axios from "axios";
import { createRoot } from "react-dom/client";
import { AgChartsReact } from "ag-charts-react";

function App() {
  const [options, setOptions] = useState({
    data: [
      { value: 45 },
      { value: 45 },
      { value: 45 },
      { value: 45 },
      { value: 45 },
      { value: 45 },
      { value: 45 },
      { value: 45 },
    ],
    series: [
      {
        type: "pie",
        angleKey: "value",
      },
    ],
  });
  return (
    <div className="App">
      <CreateForm />
      <GetBalance />
      <h3>Graph</h3>
      <AgChartsReact options={options} />
    </div>
  );
}

export default App;

const CreateForm = (props) => {
  const [bankDetails, setBankDetails] = useState({
    name: "",
    account_number: "",
    balance: "",
  });
  const [error, setError] = useState({
    name: "",
    account_number: "",
    balance: "",
  });

  const handleSubmit = async () => {
    if (!error.name && !error.account_number && !error.balance) {
      const response = await axios
        .post("http://localhost:5173/MockBankAccount", {
          name: bankDetails.name,
          account_number: Number(bankDetails.account_number),
          balance: Number(bankDetails.balance),
        })
        .then((data) => {
          // console.log(data.data);
          alert(data?.data?.message);
        })
        .catch((err) => {
          console.log(err);
        });
      console.log(response);
    }
  };

  return (
    <>
      <h5>Create Bank Details</h5>
      <div>
        <div>
          <label>Bank Holder Name : </label>
          <input
            value={bankDetails.name}
            onChange={(e) => {
              const value = { ...bankDetails };
              value.name = e.target.value;

              setBankDetails({
                ...value,
              });

              // setErrors
              const check_error = { ...error };
              if (e.target.value.length >= 50) {
                check_error.name = "name leathern 50 character";
                setError(check_error);
              } else {
                check_error.name = "";
                setError(check_error);
              }
            }}
            name="name"
            type="text"
          />
          <span
            style={{
              color: "red",
            }}
          >
            {error.name}
          </span>
        </div>
        <div>
          <label>Bank Account Number : </label>
          <input
            onChange={(e) => {
              const value = { ...bankDetails };
              value.account_number = e.target.value;
              setBankDetails({ ...value });

              // setErrors
              const check_error = { ...error };
              if (e.target.value.length != 16) {
                if (Number(e.target.value)) {
                  check_error.account_number = "must required 16 character";
                  setError(check_error);
                } else {
                  check_error.account_number = "accepted number only";
                  setError(check_error);
                }
              } else {
                check_error.account_number = "";
                setError(check_error);
              }
            }}
            value={bankDetails.account_number}
            name="account_number"
            type="text"
          />
          <span
            style={{
              color: "red",
            }}
          >
            {error.account_number}
          </span>
        </div>
        <div>
          <label>Bank Balance : </label>
          <input
            onChange={(e) => {
              const value = { ...bankDetails };
              value.balance = e.target.value;
              setBankDetails({
                ...value,
              });

              // setErrors
              const check_error = { ...error };

              if (Number(e.target.value)) {
                check_error.balance = "";
                setError(check_error);
              } else {
                check_error.balance = "accepted number or float";
                setError(check_error);
              }
            }}
            value={bankDetails.balance}
            name="balance"
            type="float"
          />
          <span
            style={{
              color: "red",
            }}
          >
            {error.balance}
          </span>
        </div>
        <div>
          <button onClick={handleSubmit}>Submit</button>
        </div>
      </div>
    </>
  );
};

const GetBalance = (props) => {
  const [account_number, setAccountNumber] = useState("");
  const [error, setError] = useState("");
  const [data, setData] = useState({});

  const handleSubmit = async () => {
    if (!error) {
      await axios
        .get(
          "http://localhost:5173/balanceapi?account_number=" + account_number
        )
        .then((data) => {
          setData(data?.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  return (
    <>
      <h5>Fetch Bank Balance</h5>
      <div>
        <div>
          <label>Bank Account Number : </label>
          <input
            name="account_number"
            value={account_number}
            onChange={(e) => {
              setAccountNumber(e.target.value);

              // set errors

              if (e.target.value.length != 16) {
                if (Number(e.target.value)) {
                  setError("must required 16 character");
                } else {
                  setError("accepted number only");
                }
              } else {
                setError("");
              }
            }}
            type="text"
          />
          <span
            style={{
              color: "red",
            }}
          >
            {error}
          </span>
        </div>
        <div>
          <button onClick={handleSubmit}>Fetch Balance</button>
        </div>
        <h3>Bank Balance</h3>
        {data.status ? (
          <>
            <h4>Name : {data.data.name}</h4>
            <h4>Account Number : {data.data.account_number}</h4>
            <h4>Balance : {data.data.balance}</h4>
          </>
        ) : data.message ? (
          <h4>{data?.message}</h4>
        ) : (
          ""
        )}
      </div>
    </>
  );
};
